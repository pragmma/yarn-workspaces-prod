## [1.0.1](https://gitlab.com/pragmma/yarn-workspaces-prod/compare/v1.0.0...v1.0.1) (2020-01-15)


### Bug Fixes

* **usage:** Adding basic example to use ywp ([4181d2a](https://gitlab.com/pragmma/yarn-workspaces-prod/commit/4181d2abfda08877b6a732a710347e32c10fbf24))

# 1.0.0 (2020-01-15)


### Features

* **build:** Adding build command ([fd6d974](https://gitlab.com/pragmma/yarn-workspaces-prod/commit/fd6d97435714bf82d24fbc669e80d534029be4d2))
* **generic:** Removing harcoded scope ([d5c2efa](https://gitlab.com/pragmma/yarn-workspaces-prod/commit/d5c2efa6daa48d46b7dc02d7e1cf04e6e381987b))
* **move:** Adding move command ([41333b9](https://gitlab.com/pragmma/yarn-workspaces-prod/commit/41333b9c7f7e217e67a68cd97b172997ce11aec1))
