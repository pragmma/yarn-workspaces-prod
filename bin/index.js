#!/usr/bin/env node
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const commander_1 = require("commander");
const build_1 = require("./internals/build");
const move_1 = require("./internals/move");
commander_1.version('1.0.0');
commander_1.command("build")
    .description("build workspace local dependecies (yarn build)")
    .action(() => {
    build_1.main();
});
commander_1.command("move")
    .description("move dist folder local dependecies to local node_modules")
    .action(() => {
    move_1.main();
});
commander_1.parse(process.argv);
if (!process.argv.slice(2).length) {
    commander_1.outputHelp();
}
