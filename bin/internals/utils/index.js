"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const fs_1 = require("fs");
const path_1 = require("path");
exports.PACKAGE_JSON = "package.json";
class WorkspaceHelper {
    constructor() {
        this.path = process.cwd();
        this.packageJson = require(`${this.path}/${exports.PACKAGE_JSON}`);
        this.workspaceFolders = getWorkspaceDependencies(this.path);
        this.dependencies = getLocalDependencies(this.packageJson.dependencies, this.workspaceFolders);
    }
    getDependenciesFolder() {
        return this.dependencies.map(({ folder }) => folder);
    }
    createNodeModulesFolder(packageName) {
        const nodeModulesPath = `${this.path}/node_modules`;
        const [scope, name] = packageName.split("/");
        if (name) {
            const scopeFolder = `${nodeModulesPath}/${scope}`;
            if (!fs_1.existsSync(scopeFolder)) {
                fs_1.mkdirSync(scopeFolder);
            }
        }
        return nodeModulesPath;
    }
}
exports.WorkspaceHelper = WorkspaceHelper;
function getScopePackageRegexp(scope) {
    return new RegExp(`(^${scope})\/(.*)`);
}
exports.getScopePackageRegexp = getScopePackageRegexp;
function getLocalDependencies(dependencies, workspaceDependencies) {
    //TODO find the shortest
    const dependenciesMap = {};
    workspaceDependencies.forEach((dep) => {
        dependenciesMap[dep.name] = dep;
    });
    const localDependencies = [];
    Object.keys(dependencies).forEach((packageName) => {
        const pkg = dependenciesMap[packageName];
        if (pkg) {
            localDependencies.push(pkg);
        }
    });
    return localDependencies;
}
exports.getLocalDependencies = getLocalDependencies;
function getWorkspaceDependencies(currentPath) {
    const rootPath = getRootPath(currentPath);
    //TODO check workspaces from paren package.json
    let packageJson;
    try {
        packageJson = require(`${rootPath}/${exports.PACKAGE_JSON}`);
    }
    catch (error) {
        throw new Error("Root project not found");
    }
    const { workspaces } = packageJson;
    const workspaceFolders = [];
    workspaces.forEach((path) => {
        if (path.includes("*")) {
            const [folder] = path.split("/");
            const dir = `${rootPath}/${folder}`;
            const folders = fs_1.readdirSync(dir).filter((f) => fs_1.lstatSync(`${dir}/${f}`).isDirectory()).map((d) => `${dir}/${d}`);
            workspaceFolders.push(...folders);
        }
        else {
            workspaceFolders.push(path);
        }
    });
    const dependencies = workspaceFolders.map((folder) => {
        const packageJson = require(`${folder}/${exports.PACKAGE_JSON}`);
        const { name } = packageJson;
        return { name, folder };
    });
    return dependencies;
}
exports.getWorkspaceDependencies = getWorkspaceDependencies;
function getPackageName(packageName, regExp) {
    const [, , name] = packageName.match(regExp);
    return name;
}
exports.getPackageName = getPackageName;
function getRootPath(path) {
    //TODO maybe add something smarter
    const parentPath = path_1.dirname(path);
    if (fs_1.existsSync(`${parentPath}/${exports.PACKAGE_JSON}`)) {
        return parentPath;
    }
    else {
        if (path == "/") {
            return path;
        }
        return getRootPath(parentPath);
    }
}
exports.getRootPath = getRootPath;
