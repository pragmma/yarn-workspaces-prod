"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const child_process_promise_1 = require("child-process-promise");
const utils_1 = require("./utils");
//TODO param for custom scope or list of dependecies
function main() {
    return __awaiter(this, void 0, void 0, function* () {
        const helper = new utils_1.WorkspaceHelper();
        const dependenciesFolders = helper.getDependenciesFolder();
        console.log(dependenciesFolders);
        //RECURSIVE DEPENDENCY
        const buildPromises = dependenciesFolders.map((path) => {
            return child_process_promise_1.exec("yarn build", {
                cwd: path
            });
        });
        try {
            const outputs = yield Promise.all(buildPromises);
            //TODO check sterr
            outputs.forEach(out => console.log(out.stderr));
            //TODO check package of each one to know the build path
            //TODO add build recursive
            //TODO do something with the dist?? pack it link it in node modules
        }
        catch (error) {
            //TODO add some colors
            console.error(error);
        }
    });
}
exports.main = main;
