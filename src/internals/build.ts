import { exec } from "child-process-promise";
import { WorkspaceHelper } from "./utils";

//TODO param for custom scope or list of dependecies

export async function main() {
    const helper = new WorkspaceHelper();
    const dependenciesFolders = helper.getDependenciesFolder();
    console.log(dependenciesFolders);
    //RECURSIVE DEPENDENCY
    const buildPromises = dependenciesFolders.map((path: string) => {
        return exec("yarn build", {
            cwd: path
        });
    });

    try {
        const outputs = await Promise.all(buildPromises);
        //TODO check sterr
        outputs.forEach(out => console.log(out.stderr));

        //TODO check package of each one to know the build path
        //TODO add build recursive
        //TODO do something with the dist?? pack it link it in node modules
    } catch (error) {
        //TODO add some colors
        console.error(error);
    }
}
