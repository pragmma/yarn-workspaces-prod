import { existsSync, mkdirSync, readdirSync, lstatSync } from "fs";
import { dirname } from "path";

export const PACKAGE_JSON = "package.json"
type IPackage = {
    name: string
    folder: string
}
export class WorkspaceHelper {
    public path: string
    public packageJson: any
    public workspaceFolders: IPackage[]
    public dependencies: { name: string, folder: string }[]

    constructor() {
        this.path = process.cwd();
        this.packageJson = require(`${this.path}/${PACKAGE_JSON}`);

        this.workspaceFolders = getWorkspaceDependencies(this.path);


        this.dependencies = getLocalDependencies(this.packageJson.dependencies, this.workspaceFolders);
    }

    getDependenciesFolder() {
        return this.dependencies.map(({ folder }) => folder);
    }
    createNodeModulesFolder(packageName: string) {
        const nodeModulesPath = `${this.path}/node_modules`;
        const [scope, name] = packageName.split("/");
        if (name) {
            const scopeFolder = `${nodeModulesPath}/${scope}`;
            if (!existsSync(scopeFolder)) {
                mkdirSync(scopeFolder);
            }
        }

        return nodeModulesPath;
    }
}

export function getScopePackageRegexp(scope: string) {
    return new RegExp(`(^${scope})\/(.*)`);
}
export function getLocalDependencies(dependencies: { [key: string]: string }, workspaceDependencies: IPackage[]): IPackage[] {
    //TODO find the shortest
    const dependenciesMap: { [key: string]: IPackage } = {};
    workspaceDependencies.forEach((dep) => {
        dependenciesMap[dep.name] = dep;
    });
    const localDependencies: IPackage[] = [];
    Object.keys(dependencies).forEach((packageName) => {
        const pkg = dependenciesMap[packageName];
        if (pkg) {
            localDependencies.push(pkg)
        }
    })

    return localDependencies;
}
export function getWorkspaceDependencies(currentPath: string): IPackage[] {

    const rootPath = getRootPath(currentPath);
    //TODO check workspaces from paren package.json
    let packageJson;
    try {
        packageJson = require(`${rootPath}/${PACKAGE_JSON}`)
    } catch (error) {
        throw new Error("Root project not found");
    }
    const { workspaces } = packageJson;
    const workspaceFolders: string[] = []
    workspaces.forEach((path: string) => {
        if (path.includes("*")) {
            const [folder] = path.split("/");
            const dir = `${rootPath}/${folder}`;
            const folders = readdirSync(dir).filter((f) => lstatSync(`${dir}/${f}`).isDirectory()).map((d) => `${dir}/${d}`);
            workspaceFolders.push(...folders);
        } else {
            workspaceFolders.push(path)
        }
    });
    const dependencies = workspaceFolders.map((folder: string) => {
        const packageJson = require(`${folder}/${PACKAGE_JSON}`);
        const { name } = packageJson;
        return { name, folder };
    })
    return dependencies;
}
export function getPackageName(packageName: string, regExp: RegExp) {
    const [, , name] = packageName.match(regExp)!;
    return name;
}

export function getRootPath(path: string): string {
    //TODO maybe add something smarter
    const parentPath = dirname(path)
    if (existsSync(`${parentPath}/${PACKAGE_JSON}`)) {
        return parentPath;
    } else {
        if (path == "/") {
            return path
        }
        return getRootPath(parentPath);
    }
}