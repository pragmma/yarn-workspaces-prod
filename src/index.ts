#!/usr/bin/env node


import { version, parse, outputHelp, command } from "commander";

import { main as mainBuild } from "./internals/build";
import { main as mainMove } from "./internals/move";


version('1.0.0');

command("build")
    .description("build workspace local dependecies (yarn build)")
    .action(() => {
        mainBuild()
    });

command("move")
    .description("move dist folder local dependecies to local node_modules")
    .action(() => {
        mainMove()
    });


parse(process.argv);



if (!process.argv.slice(2).length) {
    outputHelp();
}