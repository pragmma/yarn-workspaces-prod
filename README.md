# ywp

A CLI tool to manage workspace dependecies for production.

## ⤴️ Motivation

Yarn workspaces is great for linking local dependencies but sometines it becomes a pain for releasing to production.
This is especially true if you dont use a bundler or if your target is a node environment.
This is part of some scripts that I used in multiple projects and maybe is usefull for other people

## 💿 Installation

```bash
$ yarn add yarn-workspaces-prod --dev
```

## 📖 Usage

### CLI Commands

This `ywp` package provides 1 CLI command for now.

-   [ywp]

### ywp

To build all the yarn workspaces local dependecies from a package run `ywp build` on the package folder
This would read the _package.json_ information to detect the dependencies and run `yarn build` on each one of them (it assumes 'dist' is the out folder)
Run `ywp move` to move the _'dist'_ folder to the local node_modules allowing the transpiled code to resolve properly

## 🍻 Contributing

Welcome♡

### Bug Reports or Feature Requests

Please use GitLab Issues.

### Implementing

Please use GitLab Merge Requests.
